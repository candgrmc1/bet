import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RateService {

  constructor(private httpClient: HttpClient) { }

  getOdd(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetOdd',{},{headers}).toPromise();
  }
  addOdd(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateOddType',data,{headers});
  }

  editOdd(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateOddType',data,{headers});

  }

  getUserOdd(){
    
  }
}
