import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MatchService {

  constructor(private httpClient: HttpClient) { }

  getMatch(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetMatch',{},{headers}).toPromise();
  }

  addMatch(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'EditMatch',data,{headers});
  }

  editMatch(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateOddType',data,{headers});

  }
}
