import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BetSubTypeService {

  constructor(private httpClient: HttpClient) { }

  getBetSubTypes(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetOddField',{},{headers}).toPromise();
  }
  addBetSubType(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateOddField',{headers});
  }

  editBetSubType(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateOddField',data,{headers});

  }
}
