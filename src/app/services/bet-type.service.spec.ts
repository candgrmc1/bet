import { TestBed, inject } from '@angular/core/testing';

import { BetTypeService } from './bet-type.service';

describe('BetTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BetTypeService]
    });
  });

  it('should be created', inject([BetTypeService], (service: BetTypeService) => {
    expect(service).toBeTruthy();
  }));
});
