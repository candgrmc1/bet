import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CreditService {

  constructor(private httpClient: HttpClient) { }

  sendToUser(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'SendCreditUser',data,{headers}).toPromise();
  }
  getFromUser(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'WithdrawCreditUser' ,data,{headers}).toPromise();
  }
  sendToDealer(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'SendCreditOperator' ,data,{headers}).toPromise();
  }
  getFromDealer(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'WithdrawCreditOperator' ,data,{headers}).toPromise();
  }

  getUserTransaction(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetUserTransaction' ,{},{headers}).toPromise();

  }

}
