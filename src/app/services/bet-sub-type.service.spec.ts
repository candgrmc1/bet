import { TestBed, inject } from '@angular/core/testing';

import { BetSubTypeService } from './bet-sub-type.service';

describe('BetSubTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BetSubTypeService]
    });
  });

  it('should be created', inject([BetSubTypeService], (service: BetSubTypeService) => {
    expect(service).toBeTruthy();
  }));
});
