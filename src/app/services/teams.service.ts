import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeamsService {

  constructor(private httpClient: HttpClient) { }

  getTeams(filter={}){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetCompetitor',filter,{headers}).toPromise();
  }
  addTeam(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateCompetitor',data,{headers});
  }

  editTeam(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateCompetitor',data,{headers});

  }
}
