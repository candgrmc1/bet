import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CouponsService {

  constructor(private httpClient: HttpClient) { }

  getCoupons(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetTicket',{},{headers}).toPromise();
  }

  editCoupon(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateTicket',data,{headers}).toPromise();

  }

  addCoupon(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateTicket',data,{headers}).toPromise();
  }

  addDetailCoupon(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateTicketDetail',data,{headers}).toPromise();
  }

  getGetailCoupon(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetTicketDetail',data,{headers}).toPromise();
  }

  getUserCoupon(){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UserTickets',{},{headers}).toPromise();
  }

}
