import { TestBed, inject } from '@angular/core/testing';

import { TournementService } from './tournement.service';

describe('TournementService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TournementService]
    });
  });

  it('should be created', inject([TournementService], (service: TournementService) => {
    expect(service).toBeTruthy();
  }));
});
