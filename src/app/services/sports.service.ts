import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType,HttpHeaders } from '@angular/common/http';
import { environment as Env } from '../../environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SportsService {

  constructor(private httpClient: HttpClient) { }

  getSports(filter){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'GetSport?SportId=1',{},{headers});
  }

  addSport(data){
    console.log(data)
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'CreateSport?SportId=' + data.SportId,data,{headers});
  }

  editSport(data){
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                               .set('x-access-token',  localStorage.getItem('accessToken'));
    return this.httpClient.post(Env.api.host + 'UpdateSport?SportId=' + data.SportId,data,{headers});

  }



}
