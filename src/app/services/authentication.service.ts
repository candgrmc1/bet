
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { environment as Env } from '../../environments/environment';
import { AuthService } from 'ngx-auth';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';

import 'rxjs/add/operator/map';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService  {

  constructor(private httpClient: HttpClient, private router: Router) { }


  login(LoginName,LoginPass){
    return this.httpClient.post(Env.api.host + 'LoginOperator', {
      LoginName,
      LoginPass
    }).subscribe( (res:any) => {
      if(res.status == 'OK'){
        localStorage.setItem('accessToken',res.token)

        this.router.navigate(['/pages/dashboard'])
      }
    })
  }

  public isAuthorized(): Observable<boolean> {
    const isAuthorized: boolean = !!localStorage.getItem('accessToken');

    return Observable.of(isAuthorized);
  }
  public getAccessToken(): Observable<string> {
    const accessToken: string = localStorage.getItem('accessToken');

    return Observable.of(accessToken);
}

public refreshToken(): Observable<any> {
    const refresh_token: string = localStorage.getItem('refreshToken');

    return this.httpClient.post(this.httpClient + '/refresh_token', { refresh_token });
}

public refreshShouldHappen(response: HttpErrorResponse): boolean {
    console.log(response)
    return response.status === 401;
}

public verifyTokenRequest(url: string): boolean {
    return url.endsWith('refresh-token');
}

public async logMeout() {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('accessToken');
    this.router.navigate([''])
    return true

}

}
