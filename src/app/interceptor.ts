import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import { AuthenticationService } from './services/authentication.service';
import { Injectable } from "@angular/core";
import { tap } from "rxjs/operators";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";

@Injectable()
export class Interceptor implements HttpInterceptor {
  config = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    timeout: 3000,
    newestOnTop: true,
    tapToDismiss: true,
    preventDuplicates: false,
    animation: 'flyRight',
    limit: 5,
  });
  constructor(private router: Router,private authenticationService: AuthenticationService, private toastrService: ToasterService) { }
  //function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
   if(this.authenticationService.isAuthorized()){
      //how to update the request Parameters
    const updatedRequest = request.clone({
      headers: request.headers.set("x-access-token", localStorage.getItem('accessToken'))
    });
    //logging the updated Parameters to browser's console
    console.log("Before making api call : ", updatedRequest);
    return next.handle(request).pipe(
      tap(
        event => {
          //logging the http response to browser's console in case of a success
          if (event instanceof HttpResponse) {
            if(event.body.message){
              this.toastrService.pop('success', 'Başarılı işlem', event.body.message);
            }
            console.log("api call success :", event);
          }
        },
        error => {
          //logging the http response to browser's console in case of a failuer
          if (error.status === 401) {

            this.authenticationService.logMeout();

        }

        this.toastrService.pop('danger', 'Hata', error.error.message);

          // if (event instanceof HttpResponse) {
          //   console.log("api call error :", event);
          // }
        }
      )
    );
   }
  }


}
