import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
@Component({
  selector: 'ngx-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users:any;
  editing = {};
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this.userService.getUsers().subscribe((res:any)=>{
      this.users = res.result;
      console.log(res.result);
    })
  }
  onCreateConfirm(event){
    let data = event.newData;
    this.userService.addUser(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.userService.editUser(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

}
