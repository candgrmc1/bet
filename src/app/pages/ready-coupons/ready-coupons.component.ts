import { Component, OnInit } from '@angular/core';
import { CouponsService } from '../../services/coupons.service';
import { OperatorService } from '../../services/operator.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'ngx-ready-coupons',
  templateUrl: './ready-coupons.component.html',
  styleUrls: ['./ready-coupons.component.scss']
})
export class ReadyCouponsComponent implements OnInit {
  editing = {};
  coupons:any;
  operators = [];
  users = [];
  constructor(
    private couponsService: CouponsService,
    private operatorService: OperatorService,
    private userService: UserService,

    ) { }

  ngOnInit() {
    this.getOperators()
    this.getUsers();
    this.getReadyCoupons()
  }

  getReadyCoupons(){
    this.couponsService.getCoupons().then((res:any)=>{
      if(res.status=='OK'){
        this.coupons = res.result;
      }
    })
  }

  getUsers(){
    this.userService.getUsers().toPromise().then((res:any)=>{
      console.log(res)
      if(res.status === 'OK'){
        res.result.map((user)=>{
          this.users[user.UserId] = user.LoginName
        })
      }
    })
  }
  getOperators(){
    this.operatorService.getOperators().then((res:any)=>{
      console.log(res)
      if(res.status === 'OK'){
        res.result.map((operator)=>{
          this.operators[operator.OperatorId] = operator.LoginName
        })
      }
    })
  }

}
