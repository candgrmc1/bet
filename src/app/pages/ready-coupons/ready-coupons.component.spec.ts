import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReadyCouponsComponent } from './ready-coupons.component';

describe('ReadyCouponsComponent', () => {
  let component: ReadyCouponsComponent;
  let fixture: ComponentFixture<ReadyCouponsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadyCouponsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadyCouponsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
