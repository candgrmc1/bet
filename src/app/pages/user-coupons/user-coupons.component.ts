import { Component, OnInit } from '@angular/core';
import { CouponsService } from '../../services/coupons.service';

@Component({
  selector: 'ngx-user-coupons',
  templateUrl: './user-coupons.component.html',
  styleUrls: ['./user-coupons.component.scss']
})
export class UserCouponsComponent implements OnInit {
  editing = {};
  coupons:any;
  constructor(private couponsService: CouponsService) { }

  ngOnInit() {
    this.getUserCoupons();
  }

  getUserCoupons(){
    this.couponsService.getUserCoupon().then((res:any)=>{
      console.log(res)
    })
  }

}
