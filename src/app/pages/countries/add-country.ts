import { Component, Output, EventEmitter, Input,OnInit } from '@angular/core';
import { SportsService } from '../../services/sports.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CountriesService } from '../../services/countries.service';

@Component({
  selector: 'ngx-add-countries',
  templateUrl: './add-country.html',
  styleUrls: ['./countries.component.scss']
})
export class AddCountry implements OnInit  {
  @Input() CountryId:number;
  @Input() Title:string;
  @Input() Sport:number;
  @Input() Status:number;
  @Input() ListOrder:number;
  sports:any;
  selected = [{

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }]
    options = [
      {

        "index": 0,
        "value": 0 ,
        "name": "Pasif"
      },
      {

        "index": 1,
        "value": 1 ,
        "name": "Aktif"
      }
    ]

    config = {
      displayKey: "name",
      search: true,
      limitTo: 3,
      placeholder:'Durum'
    };

    configSport = {
      displayKey: "Title",
      search: true,
      limitTo: 10,
      placeholder:'Sport'
    }
  constructor(
    public activeModal: NgbActiveModal,
    private sportsService: SportsService,
    private countryService: CountriesService
  ) { }

  ngOnInit() {
    this.getSports({});
  }
  changeSport(event){
    this.Sport = event.value[0].SportId
  }
  change(event){
    this.Status = event.value[0].value
  }

  saveItem(){
    let newRow = {CategoryId:this.CountryId,Title:this.Title,SportId:this.Sport,Status:this.Status,ListOrder:this.ListOrder};
    this.countryService.addCountry(newRow).then((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.activeModal.close(true)
      }

    })
  }

  getSports(filter){
    this.sportsService.getSports(filter).toPromise().then((res:any)=>{
      if(res.status === 'OK'){
        this.sports = res.result;
      }
    })
  }

  closeModal() {
    this.activeModal.close(false);
  }
}
