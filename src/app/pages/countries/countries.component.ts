import { AddCountry } from './add-country';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { LocalDataSource } from 'ng2-smart-table';
import { Component, OnInit, ViewChild,AfterContentInit } from '@angular/core';
import { CountriesService } from '../../services/countries.service';
import { SportsService } from '../../services/sports.service';

@Component({
  selector: 'ngx-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {
  @ViewChild(DatatableComponent) private table: DatatableComponent
  modal: NgbActiveModal;
  countries:any;
  editing = {};
  sportsList = [];
  sports:any;
  search:any;
  Title:string;
  Status:number;
  ListOrder:number;
  CategoryId:number;
  SportId:number;
  sportFilter:number
  config = {
    displayKey: "name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 3,
    placeholder:'Durum'
  };
  configSportFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 3,
    placeholder:'Spor'
  };
  // selected = [{

  //   "index": 1,
  //   "value": 1 ,
  //   "name": "Aktif"
  // }]
  options = [
    {

      "index": 0,
      "value": 0 ,
      "name": "Pasif"
    },
    {

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }
  ]
  public currentPageLimit: number = 25;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];
  constructor(private countriesService: CountriesService, private sportsService: SportsService,private modalService: NgbModal) {

   }

  ngOnInit() {
    this.getSports({});
    this.getCountries();
  }

  public onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  private changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  filterSport(event){
    console.log(event.value[0].SportId)
    this.getCountries({SportId:event.value[0].SportId})
  }
  onKeydownEvent(event: KeyboardEvent,key,value): void {
    if (event.keyCode === 13) {
        if(value && value != '' ) {
          this.search[key] = value
          this.getCountries(this.search)
        }else{
          this.getCountries({});
        }
    }
 }
  getCountries(filter = {}){
    this.countriesService.getCountries(filter).then((res:any)=>{
      if(res.status === 'OK'){
       this.countries = res.result
        console.log(this.countries)
      }
    })
  }



  getSports(filter){
    this.sportsService.getSports(filter).subscribe((res:any)=>{
      if(res.status == 'OK'){
        this.sports = res.result;
      }
    },
    (error) => {
      console.log(error)
    }
    )
  }

  openFormModal() {
    const modalRef = this.modalService.open(AddCountry,{size:'lg'});

    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        this.getCountries()

      };
    }).catch((error) => {
      console.log(error);
    });
  }

  onCreateConfirm(){
    this.countriesService.addCountry({
      CategoryId:this.CategoryId,
      Title:this.Title,
      Status:this.Status,
      ListOrder:this.ListOrder,
      SportId:this.SportId
    }).then((res:any)=>{
      if(res.status=='OK'){
        this.getCountries();
        this.Title = null;
        this.Status = null;
        this.ListOrder = null;
        this.CategoryId = null;
        this.SportId = null;
      }
    })

  }

  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.countries[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.countriesService.editCountry(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        if(cell === 'SportId'){
          console.log(this.countries[rowIndex].SportId)
          this.sports.map((s)=>{
            if(s.SportId === parseInt(this.countries[rowIndex].SportId)){
              console.log('title:'+s.Title)
              this.countries[rowIndex]['Sport'] = s.Title
            }
          })
        }
        this.countries[rowIndex][cell] = parseInt(event.target.value);
        this.countries = [...this.countries];
      }
    });

    console.log('UPDATED!', this.countries[rowIndex][cell]);
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.countriesService.editCountry(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.countriesService
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }

  onChange(event,rowIndex){
    let r = this.countries[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.countriesService.editCountry(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.countries[rowIndex][cell] = st;
        this.countries = [...this.countries];
      }
    });
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
