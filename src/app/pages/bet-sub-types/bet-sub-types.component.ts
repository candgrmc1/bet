import { Component, OnInit } from '@angular/core';
import { BetSubTypeService } from '../../services/bet-sub-type.service';
import { LocalDataSource } from 'ng2-smart-table';
import { BetTypeService } from '../../services/bet-type.service';

@Component({
  selector: 'ngx-bet-sub-types',
  templateUrl: './bet-sub-types.component.html',
  styleUrls: ['./bet-sub-types.component.scss']
})
export class BetSubTypesComponent implements OnInit {
  subTypes:any;
  editing = {};
  oddTypes = [];
  constructor(private betSubTypeService: BetSubTypeService,private betTypeService: BetTypeService) { }
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      ListOrder: {
        title: 'ListOrder',
        type: 'number',
      },
      OddFieldId: {
        title: 'OddFieldId',
        type: 'number',
      },
      OddType: {
        title: 'OddType',
        type: 'string',
      },
      OddTypeId: {
        title: 'OddTypeId',
        type: 'number',
      },
      Status: {
        title: 'Status',
        type: 'number',
      },
      Title: {
        title: 'Title',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();
  ngOnInit() {
    this.getBetTypes();
    this.getBetSubTypes();
  }

  getBetSubTypes(){
    this.betSubTypeService.getBetSubTypes().then((res:any)=>{
      if(res.status === 'OK'){
      this.subTypes = res.result;
      this.source.load(res.result);
      console.log(this.subTypes);
      }
    })
  }
  getBetTypes(){
    this.betTypeService.getBetTypes().then((res:any)=>{
      if(res.status == 'OK'){
        res.result.map((type)=>{
          this.oddTypes[type.OddTypeId] = type.Title
        })
      }
    })
  }
  onCreateConfirm(event){
    let data = event.newData;
    this.betSubTypeService.addBetSubType(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.betSubTypeService.editBetSubType(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.subTypes[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.betSubTypeService.editBetSubType(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.subTypes[rowIndex][cell] = event.target.value;
        this.subTypes = [...this.subTypes];
      }
    });

    console.log('UPDATED!', this.subTypes[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.subTypes[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.betSubTypeService.editBetSubType(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.subTypes[rowIndex][cell] = st;
        this.subTypes = [...this.subTypes];
      }
    });
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
