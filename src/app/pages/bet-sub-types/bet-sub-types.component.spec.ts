import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetSubTypesComponent } from './bet-sub-types.component';

describe('BetSubTypesComponent', () => {
  let component: BetSubTypesComponent;
  let fixture: ComponentFixture<BetSubTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetSubTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetSubTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
