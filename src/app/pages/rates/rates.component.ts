import { BetTypeService } from './../../services/bet-type.service';
import { LocalDataSource } from 'ng2-smart-table';
import { RateService } from './../../services/rate.service';
import { Component, OnInit } from '@angular/core';
import { MatchService } from '../../services/match.service';

@Component({
  selector: 'ngx-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  rates:any;
  editing = {};
  matches = [];
  betTypes = [];
  constructor(private rateService: RateService,private matchService: MatchService,private betTypeService: BetTypeService) { }

  ngOnInit() {
    this.getMatches();
    this.getBetTypes();
    this.getRates();
  }

  getRates(){
    this.rateService.getOdd().then((res:any)=>{
      if(res.status==='OK'){
        this.rates = res.result;
      }
    })
  }
  getBetTypes(){
    this.betTypeService.getBetTypes().then((res:any) => {
      if(res.status === 'OK'){
        res.result.map((type)=>{
          this.betTypes[type.OddTypeId] = type.Title
        })
      }
    })
  }
  getMatches(){
    this.matchService.getMatch().then((res:any)=>{
      if(res.status === 'OK'){
        res.result.map((match:any)=>{
          this.matches[match.MatchId] = match.Comp1 + '-' + match.Comp2
        })
      }
    })
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onCreateConfirm(event){
    let data = event.newData;
    this.rateService.addOdd(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.rates[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.rateService.editOdd(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.rates[rowIndex][cell] = event.target.value;
        this.rates = [...this.rates];
      }
    });

    console.log('UPDATED!', this.rates[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.rates[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.rateService.editOdd(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.rates[rowIndex][cell] = st;
        this.rates = [...this.rates];
      }
    });
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.rateService.editOdd(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
}
