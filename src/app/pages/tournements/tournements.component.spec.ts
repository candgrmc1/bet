import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournementsComponent } from './tournements.component';

describe('TournementsComponent', () => {
  let component: TournementsComponent;
  let fixture: ComponentFixture<TournementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournementsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
