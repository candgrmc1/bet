import { LocalDataSource } from 'ng2-smart-table';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Component, OnInit, AfterContentInit,ViewChild } from '@angular/core';
import { TournementService } from '../../services/tournement.service';
import { CountriesService } from '../../services/countries.service';
import { SportsService } from '../../services/sports.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddTournement } from './add-tournement';

@Component({
  selector: 'ngx-tournements',
  templateUrl: './tournements.component.html',
  styleUrls: ['./tournements.component.scss']
})
export class TournementsComponent implements OnInit, AfterContentInit {
  @ViewChild(DatatableComponent) private table: DatatableComponent

  tournements:any;
  countries=[];
  selectCountries:any;
  sports:any;
  search = {
    SportId: null,
    CategoryId: null,
    Status: null
  };
  editing = {};
  CategoryId:number=0;
  SportId:number=0;
  Status:number=0;
  Title:string;
  ListOrder:number;
  TournamentId:number;
  configSportFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Spor'
  };
  configCountryFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Ülke'
  };
  configStatusFilter = {
    displayKey: "name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Durum'
  };
  configPageLimit = {
    displayKey: "value", //if objects array passed which key to be displayed defaults to description
    search: false,
    limitTo: 20,
    placeholder:'Limit'
  };
  statuses = [
    {

      "index": 0,
      "value": 0 ,
      "name": "Pasif"
    },
    {

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }
  ]
  public currentPageLimit: number = 25;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];

  selectedSportFilter;
  selectedCountryFilter;
  selectedStatusFilter;
  constructor(
    private tournementService: TournementService,
    private countriesService: CountriesService,
    private sportsService:SportsService,
    private modalService: NgbModal
    ) { }

  ngOnInit() {
    this.getSports()
    this.getCountries({});
  }
  ngAfterContentInit(){
    this.getTournements();
  }
  public onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }
  private changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }
  filterBySport(event){
    console.log(event)
    delete(this.search['CategoryId'])
    this.selectedSportFilter = event.value;
    this.search.SportId = event.value[0].SportId
    this.getTournements(this.search)
    this.selectCountries = []
    this.selectedCountryFilter = null;
    this.getCountriesForSelect({SportId:event.value[0].SportId})
  }

  filterByCountry(event,){
    this.selectedCountryFilter = event.value
    this.search.CategoryId = event.value[0].SportId
    console.log(this.search)
    this.getTournements(this.search)
  }

  filterByStatus(event,){
    this.search.Status = event.value[0].value
    console.log(this.search)
    this.getTournements(this.search)
  }

  getTournements(filter={}){
    this.tournementService.getTournaments(filter).then((res:any) => {
        if(res.status=='OK'){


        this.tournements = res.result
        console.log(this.tournements);
        }
    })
  }
  onCreateConfirm(){
    this.tournementService.addTournament({
      CategoryId:this.CategoryId,
      SportId: this.SportId,
      Status: this.Status,
      ListOrder: this.ListOrder,
      Title: this.Title,
      TournamentId: this.TournamentId
    }).then((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.getTournements();
        this.CategoryId = null;
        this.SportId = null;
        this.Status = null;
        this.ListOrder = null;
        this.Title = null
        this.TournamentId = null;
      }
    })
  }
  getCountries(filter){
    this.countriesService.getCountries(filter).then((res:any)=>{
      if(res.status === 'OK'){
        console.log(res.result)
        this.countries = res.result


      }
    })
  }
  getCountriesForSelect(filter){
    this.countriesService.getCountries(filter).then((res:any)=>{
      if(res.status === 'OK'){
        console.log(res.result)
        this.selectCountries = res.result
      }
    })
  }
  getSports(filter={}){
    this.sportsService.getSports(filter).subscribe((res:any)=>{
      if(res.status == 'OK'){
        this.sports = res.result;
      }
    },
    (error) => {
      console.log(error)
    }
    )
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.tournements[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.tournementService.editTournament(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        if(cell === 'SportId'){
          this.sports.map((s)=>{
            if(s.SportId === parseInt(this.tournements[rowIndex].SportId)){
              console.log('title:'+s.Title)
              this.tournements[rowIndex]['Sport'] = s.Title
            }
          })
        }
        if(cell === 'CategoryId'){
          this.countries.map((s)=>{
            if(s.CategoryId === parseInt(this.tournements[rowIndex].CategoryId)){
              console.log('title:'+s.Title)
              this.tournements[rowIndex]['Category'] = s.Title
            }
          })
        }
        this.editing[rowIndex + '-' + cell] = false;
        this.tournements[rowIndex][cell] = event.target.value;
        this.tournements = [...this.tournements];
      }
    });

    console.log('UPDATED!', this.tournements[rowIndex][cell]);
  }

  openFormModal() {
    const modalRef = this.modalService.open(AddTournement,{size:'lg'});

    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        this.getTournements()

      };
    }).catch((error) => {
      console.log(error);
    });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
