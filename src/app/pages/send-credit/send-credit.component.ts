import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'ngx-send-credit',
  templateUrl: './send-credit.component.html',
  styleUrls: ['./send-credit.component.scss']
})
export class SendCreditComponent implements OnInit {
  users=[];
  serviceType='user';
  constructor(private userService: UserService) { }

  ngOnInit() {
    //this.getUserList();
  }

  getUserList(){
    this.userService.getUserList().then((res:any)=>{
      console.log(res);
      if(res.status === 'OK'){
        res.result.map((user)=>{
          this.users[user.UserId]=user.LoginName
        })
      }
    })
  }
}
