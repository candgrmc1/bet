import { Component, OnInit } from '@angular/core';
import { OperatorService } from '../../services/operator.service';

@Component({
  selector: 'ngx-dealers',
  templateUrl: './dealers.component.html',
  styleUrls: ['./dealers.component.scss']
})
export class DealersComponent implements OnInit {

  constructor(private operatorService: OperatorService) { }
  dealers:any;
  editing = {};
  ngOnInit() {
    this.getDealers();
  }

  getDealers(){
    this.operatorService.getOperators().then((res:any)=>{
      if(res.status==='OK'){
        this.dealers = res.result
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.dealers[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.operatorService.editOperator(r).then((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.dealers[rowIndex][cell] = event.target.value;
        this.dealers = [...this.dealers];
      }
    });

    console.log('UPDATED!', this.dealers[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.dealers[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.operatorService.editOperator(r).then((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.dealers[rowIndex][cell] = st;
        this.dealers = [...this.dealers];
      }
    });
  }

}
