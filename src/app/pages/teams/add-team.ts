import { Component, Output, EventEmitter, Input } from '@angular/core';
import { SportsService } from '../../services/sports.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-add-sports',
  templateUrl: './add-sport.component.html',
  styleUrls: ['./sports.component.scss']
})
export class AddTeam {

  @Input() SportId:number;
  @Input() Title:string;
  @Input() Status:number;
  @Input() ListOrder:number;
  selected = [{

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }]
    options = [
      {

        "index": 0,
        "value": 0 ,
        "name": "Pasif"
      },
      {

        "index": 1,
        "value": 1 ,
        "name": "Aktif"
      }
    ]

    config = {
      displayKey: "name",
      search: true,
      limitTo: 3,
      placeholder:'Durum'
    };
  constructor(
    public activeModal: NgbActiveModal,
    private sportsService: SportsService
  ) { }

  saveItem(){
    let newRow = {SportId:this.SportId,Title:this.Title,Status:this.Status,ListOrder:this.ListOrder};
    this.sportsService.addSport(newRow).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.activeModal.close(true)
      }

    })
  }
  changeStatus(event){
    this.Status = event.value[0].value
  }

  closeModal() {
    this.activeModal.close(false);
  }
}
