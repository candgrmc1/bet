import { LocalDataSource } from 'ng2-smart-table';
import { TeamsService } from './../../services/teams.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ToasterService, ToasterConfig, Toast, BodyOutputType } from 'angular2-toaster';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { CountriesService } from '../../services/countries.service';
import { SportsService } from '../../services/sports.service';
import { TournementService } from '../../services/tournement.service';

@Component({
  selector: 'ngx-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  @ViewChild(DatatableComponent) private table: DatatableComponent

  teams:any;
  editing = {};
  sports = [];
  search = {
    SportId: null,
    CategoryId: null,
    Status: null,
    TournamentId:null
  };
  configSportFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Spor'
  };
  configCountryFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Ülke'
  };
  configTournementFilter = {
    displayKey: "Title", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Turnuva'
  };
  configStatusFilter = {
    displayKey: "name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 20,
    placeholder:'Durum'
  };
  configPageLimit = {
    displayKey: "value", //if objects array passed which key to be displayed defaults to description
    search: false,
    limitTo: 20,
    placeholder:'Limit'
  };
  statuses = [
    {

      "index": 0,
      "value": 0 ,
      "name": "Pasif"
    },
    {

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }
  ]
  public currentPageLimit: number = 25;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];

  selectedSportFilter;
  selectedCountryFilter;
  selectedStatusFilter;
  selectedTournementFilter;
  selectCountries:any;
  selectTournements:any;
  constructor(
    private teamsService: TeamsService,
    private toastrService: ToasterService,
    private countriesService: CountriesService,
    private sportsService: SportsService,
    private tournementService: TournementService
    ) { }

  ngOnInit() {
    this.getTeams();
    this.getSports();
  }

  getTeams(filter={}){
    this.teamsService.getTeams(filter).then((res:any) => {
        if(res.status === 'OK'){
          this.teams = res.result;
        }
    })
  }
  public onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }
  private changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }
  filterBySport(event){
    console.log(event)
    delete(this.search['CategoryId'])
    this.selectedSportFilter = event.value;
    this.search.SportId = event.value[0].SportId
    this.getTeams(this.search)
    this.selectCountries = []
    this.selectedCountryFilter = null;
    this.getCountriesForSelect({SportId:event.value[0].SportId})
  }

  filterByCountry(event,){
    this.selectedCountryFilter = event.value
    this.search.CategoryId = event.value[0].CategoryId
    this.selectedTournementFilter = null;
    this.selectTournements = [];
    this.search.TournamentId = null;
    this.getTeams(this.search)
    this.getTournementsForSelect({CategoryId:event.value[0].CategoryId})

  }
  filterByTournement(event,){
    this.selectedTournementFilter = event.value
    this.search.TournamentId = event.value[0].TournamentId
    console.log(this.search)
    this.getTeams(this.search)
    this.getTournementsForSelect({CategoryId:event.value[0].CategoryId})

  }

  filterByStatus(event,){
    this.search.Status = event.value[0].value
    console.log(this.search)
    this.getTeams(this.search)
  }
  getCountriesForSelect(filter){
    this.countriesService.getCountries(filter).then((res:any)=>{
      if(res.status === 'OK'){
        console.log(res.result)
        this.selectCountries = res.result
      }
    })
  }
  getTournementsForSelect(filter){
    this.tournementService.getTournaments(filter).then((res:any)=>{
      if(res.status === 'OK'){
        console.log(res.result)
        this.selectTournements = res.result
      }
    })
  }
  getSports(filter={}){
    this.sportsService.getSports(filter).subscribe((res:any)=>{
      if(res.status == 'OK'){
        this.sports = res.result;
      }
    },
    (error) => {
      console.log(error)
    }
    )
  }
  onCreateConfirm(event){

    let data = event.newData;
    this.teamsService.addTeam(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){

        this.toastrService.pop('success','Başarılı','Takım başarıyla eklendi')
        event.confirm.resolve()
      }else{
        const toast: Toast = {
          type: 'error',
          title: 'Başarısız işlem',
          body: res.message,
          showCloseButton: true,
          bodyOutputType: BodyOutputType.TrustedHtml,
        };
        this.toastrService.popAsync(toast)
        event.confirm.reject();
      }
    })
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.teamsService.editTeam(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.teams[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.teamsService.editTeam(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.teams[rowIndex][cell] = event.target.value;
        this.teams = [...this.teams];
      }
    });

    console.log('UPDATED!', this.teams[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.teams[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.teamsService.editTeam(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.teams[rowIndex][cell] = st;
        this.teams = [...this.teams];
      }
    });
  }
}
