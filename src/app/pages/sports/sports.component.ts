import { Component, OnInit, ViewChild } from '@angular/core';
import { SportsService } from '../../services/sports.service';
import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { AddSport } from './add-sport';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'ngx-sports',
  templateUrl: './sports.component.html',
  styleUrls: ['./sports.component.scss']
})
export class SportsComponent implements OnInit {
  @ViewChild(DatatableComponent) private table: DatatableComponent
  @ViewChild('content')
    modal: NgbActiveModal;
  public currentPageLimit: number = 25;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];
  input;
  editing = {};
  rows: any;
  statuses = { 0 : "Passive", 1: "Active"};
  closeResult: string;
  config = {
    displayKey: "name", //if objects array passed which key to be displayed defaults to description
    search: true,
    limitTo: 3,
    placeholder:'Durum'
  };
  // selected = [{

  //   "index": 1,
  //   "value": 1 ,
  //   "name": "Aktif"
  // }]
  options = [
    {

      "index": 0,
      "value": 0 ,
      "name": "Pasif"
    },
    {

      "index": 1,
      "value": 1 ,
      "name": "Aktif"
    }
  ]

  SportId:number;
  Title:string;
  Status:number = 1;
  ListOrder:number;
  search = {}

  constructor(private sportsService: SportsService,private modalService: NgbModal) {

   }


  ngOnInit() {
    this.getSports();
  }

  dropChange(event){

    if(event.value[0].value){
      this.getSports({Status:event.value[0].value})
    }
  }
  onKeydownEvent(event: KeyboardEvent,key,value): void {
    if (event.keyCode === 13) {
        if(value && value != '' ) {
          this.search[key] = value
          this.getSports(this.search)
        }else{
          this.getSports({});
        }
    }
 }
  public onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  private changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  openFormModal() {
    const modalRef = this.modalService.open(AddSport,{size:'lg'});

    modalRef.result.then((result) => {
      console.log(result)
      if(result){
        this.getSports()

      };
    }).catch((error) => {
      console.log(error);
    });
  }





  getSports(filter={}){
    this.sportsService.getSports(filter).toPromise().then((res:any)=>{
      if(res.status == 'OK'){
        console.log(res.result)
        this.rows = res.result
      }
    },
    (error) => {
      console.log(error)
    }
    )
  }
  onCreateConfirm(){
    console.log()
    let newRow = {SportId:this.SportId,Title:this.Title,Status:this.Status,ListOrder:this.ListOrder};
    this.sportsService.addSport(newRow).subscribe((res:any)=>{
      if(res.status=='OK'){
        this.rows[this.SportId] = newRow;
        this.rows = [...this.rows];
        this.SportId = null;
        this.Title = null;
        this.Status = null;
        this.ListOrder = null;
        this.getSports();
      }

    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.rows[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.sportsService.editSport(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.rows[rowIndex][cell] = event.target.value;
        this.rows = [...this.rows];
      }
    });

    console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

  updateValueEnter(event, cell, rowIndex) {
      if (event.keyCode === 13) {
      console.log(cell)
      console.log('inline editing rowIndex', rowIndex)
      let r = this.rows[rowIndex];
      r[cell] = event.target.value;
      console.log(r);
      this.sportsService.editSport(r).subscribe((res:any)=>{
        console.log(res)
        if(res.status=='OK'){
          this.editing[rowIndex + '-' + cell] = false;
          this.rows[rowIndex][cell] = event.target.value;
          this.rows = [...this.rows];
        }
      });

      console.log('UPDATED!', this.rows[rowIndex][cell]);
    }
  }

  onChange(event,rowIndex){
    let r = this.rows[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.sportsService.editSport(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.rows[rowIndex][cell] = st;
        this.rows = [...this.rows];
      }
    });
  }


  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}


