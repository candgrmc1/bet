import { Component, OnInit } from '@angular/core';
import { ResultService } from '../../services/result.service';
import { LocalDataSource } from 'ng2-smart-table';

@Component({
  selector: 'ngx-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {
  results:any;
  editing = {};
  constructor(private resultService: ResultService) { }
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      Comp1: {
        title: 'Comp1',
        type: 'string',
      },
      Comp2: {
        title: 'Comp2',
        type: 'string',
      },
      FtScore: {
        title: 'FtScore',
        type: 'string',
      },
      HtScore: {
        title: 'HtScore',
        type: 'string',
      },
      MatchDate: {
        title: 'MatchDate',
        type: 'string',
      },
      MatchId: {
        title: 'MatchId',
        type: 'number',
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();
  ngOnInit() {
    this.getResults();
  }

  getResults(){
    this.resultService.getResult().subscribe((res:any)=>{
      if(res.status==='OK'){
        this.results = res.result;
        this.source.load(res.result);
        console.log(this.results);
      }
    })
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  onCreateConfirm(event){
    let data = event.newData;
    this.resultService.addResult(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.resultService.editResult(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.results[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.resultService.editResult(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.results[rowIndex][cell] = event.target.value;
        this.results = [...this.results];
      }
    });

    console.log('UPDATED!', this.results[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.results[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.resultService.editResult(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.results[rowIndex][cell] = st;
        this.results = [...this.results];
      }
    });
  }
}
