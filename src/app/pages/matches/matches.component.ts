import { TeamsService } from './../../services/teams.service';
import { MatchService } from './../../services/match.service';
import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SportsService } from '../../services/sports.service';
import { TournementService } from '../../services/tournement.service';

@Component({
  selector: 'ngx-matches',
  templateUrl: './matches.component.html',
  styleUrls: ['./matches.component.scss']
})
export class MatchesComponent implements OnInit {
  matches:any;
  editing = {};
  teams = [];
  sports = [];
  tournaments = [];
  constructor(private matchService: MatchService,private teamsService: TeamsService,private sportsService: SportsService,private tournamentsService: TournementService) { }

  ngOnInit() {
    this.getTeams();
    this.getTournaments();
    this.getSports({});
    this.getMatches()
  }

  getMatches(){
    this.matchService.getMatch().then((res:any)=>{
      if(res.status === 'OK'){
        this.matches = res.result;
        console.log(this.matches)
      }
    })
  }

  getTeams(){
    this.teamsService.getTeams().then((res:any)=>{
      if(res.status === 'OK'){
        res.result.map((team)=>{
          this.teams[team.CompId] = team.Title
        })
      }
    })
  }
  getSports(filter){
    this.sportsService.getSports(filter).toPromise().then((res:any)=>{
      res.result.map((sport)=>{
        this.sports[sport.SportId] = sport.Title
      })
    })
  }
  getTournaments(){
    this.tournamentsService.getTournaments().then((res:any)=>{
      if(res.status==='OK'){
        res.result.map((tournament) =>{
          this.tournaments[tournament.TournamentId] = tournament.Title
        })
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.matches[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.matchService.editMatch(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.matches[rowIndex][cell] = event.target.value;
        this.matches = [...this.matches];
      }
    });

    console.log('UPDATED!', this.matches[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.matches[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.matchService.editMatch(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.matches[rowIndex][cell] = st;
        this.matches = [...this.matches];
      }
    });
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
