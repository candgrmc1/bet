import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BetTypesComponent } from './bet-types.component';

describe('BetTypesComponent', () => {
  let component: BetTypesComponent;
  let fixture: ComponentFixture<BetTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BetTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BetTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
