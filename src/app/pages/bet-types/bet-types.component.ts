import { LocalDataSource } from 'ng2-smart-table';
import { BetTypeService } from './../../services/bet-type.service';
import { Component, OnInit } from '@angular/core';
import { SportsService } from '../../services/sports.service';

@Component({
  selector: 'ngx-bet-types',
  templateUrl: './bet-types.component.html',
  styleUrls: ['./bet-types.component.scss']
})
export class BetTypesComponent implements OnInit {
  betTypes:any;
  editing = {};
  sports =[];
  constructor(private betTypeService: BetTypeService,private sportsService: SportsService) { }

  ngOnInit() {
    this.getSports()
    this.getBetTypes();
  }

  getBetTypes(){
    this.betTypeService.getBetTypes().then((res:any) => {
      if(res.status === 'OK'){
        this.betTypes = res.result
        console.log(this.betTypes)
      }
    })
  }

  getSports(){
    this.sportsService.getSports({}).toPromise().then((res:any)=>{
      res.result.map((sport)=>{
        this.sports[sport.SportId] = sport.Title
      })
    })
  }
  onCreateConfirm(event){
    let data = event.newData;
    this.betTypeService.addBetType(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  onConfirmEdit(event){
    let data = event.newData;
    this.betTypeService.editBetType(data).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        event.confirm.resolve()
      }else{
        event.confirm.reject();
      }
    })
  }
  updateValue(event, cell, rowIndex) {
    console.log(cell)
    console.log('inline editing rowIndex', rowIndex)
    let r = this.betTypes[rowIndex];
    r[cell] = event.target.value;
    console.log(r);
    this.betTypeService.editBetType(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.editing[rowIndex + '-' + cell] = false;
        this.betTypes[rowIndex][cell] = event.target.value;
        this.betTypes = [...this.betTypes];
      }
    });

    console.log('UPDATED!', this.betTypes[rowIndex][cell]);
  }

  onChange(event,rowIndex){
    let r = this.betTypes[rowIndex];
    let cell ='Status';
    let st = (event) ? 1 : 0;
    r[cell] = st;
    console.log(r);
    this.betTypeService.editBetType(r).subscribe((res:any)=>{
      console.log(res)
      if(res.status=='OK'){
        this.betTypes[rowIndex][cell] = st;
        this.betTypes = [...this.betTypes];
      }
    });
  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
