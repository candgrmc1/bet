import { MatchesComponent } from './matches/matches.component';
import { ReportsComponent } from './reports/reports.component';
import { RiskManagementComponent } from './risk-management/risk-management.component';
import { UserCouponsComponent } from './user-coupons/user-coupons.component';
import { DealersComponent } from './dealers/dealers.component';
import { ResultsComponent } from './results/results.component';
import { TeamsComponent } from './teams/teams.component';
import { TournementsComponent } from './tournements/tournements.component';
import { CountriesComponent } from './countries/countries.component';
import { SportsComponent } from './sports/sports.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PublicGuard, ProtectedGuard } from 'ngx-auth';


import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { BetTypesComponent } from './bet-types/bet-types.component';
import { BetSubTypesComponent } from './bet-sub-types/bet-sub-types.component';
import { RatesComponent } from './rates/rates.component';
import { UsersComponent } from './users/users.component';
import { TransfersComponent } from './transfers/transfers.component';
import { AccountComponent } from './account/account.component';
import { ReadyCouponsComponent } from './ready-coupons/ready-coupons.component';
import { UserRatesComponent } from './user-rates/user-rates.component';
import { SendCreditComponent } from './send-credit/send-credit.component';
import { WithdrawCreditComponent } from './withdraw-credit/withdraw-credit.component';
import { CreditHistoryComponent } from './credit-history/credit-history.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate:[ProtectedGuard],
  canActivateChild:[ProtectedGuard],
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'sports',
    component: SportsComponent,
  }, {
    path: 'countries',
    component: CountriesComponent,
  }, {
    path: 'tournements',
    component: TournementsComponent,
  }, {
    path: 'teams',
    component: TeamsComponent,
  }, {
    path: 'bet-types',
    component: BetTypesComponent,
  }, {
    path: 'bet-sub-types',
    component: BetSubTypesComponent,
  },
  {
    path: 'matches',
    component: MatchesComponent,
  },
  {
    path: 'rates',
    component: RatesComponent,
  }, {
    path: 'results',
    component: ResultsComponent,
  }, {
    path: 'dealers',
    component: DealersComponent,
  }, {
    path: 'users',
    component: UsersComponent,
  }, {
    path: 'transfers',
    component: TransfersComponent,
  },
  {
    path: 'send-credit',
    component: SendCreditComponent,
  },
  {
    path: 'withdraw-credit',
    component: WithdrawCreditComponent,
  },
  {
    path: 'credit-history',
    component: CreditHistoryComponent,
  },
  {
    path: 'account',
    component: AccountComponent,
  }, {
    path: 'ready-coupons',
    component: ReadyCouponsComponent,
  }, {
    path: 'user-coupons',
    component: UserCouponsComponent,
  }, {
    path: 'user-rates',
    component: UserRatesComponent,
  }, {
    path: 'risk-management',
    component: RiskManagementComponent,
  }, {
    path: 'reports',
    component: ReportsComponent,
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
