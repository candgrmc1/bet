import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private autenticationService:AuthenticationService) { }

  ngOnInit() {
  }


  login(username,password){
    this.autenticationService.login(username,password)
  }

}
