import { AddTournement } from './tournements/add-tournement';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ToasterModule, ToasterService } from 'angular2-toaster';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { SportsComponent } from './sports/sports.component';
import { CountriesComponent } from './countries/countries.component';
import { TournementsComponent } from './tournements/tournements.component';
import { TeamsComponent } from './teams/teams.component';
import { BetTypesComponent } from './bet-types/bet-types.component';
import { BetSubTypesComponent } from './bet-sub-types/bet-sub-types.component';
import { MatchesComponent } from './matches/matches.component';
import { RatesComponent } from './rates/rates.component';
import { ResultsComponent } from './results/results.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DealersComponent } from './dealers/dealers.component';
import { UsersComponent } from './users/users.component';
import { AccountComponent } from './account/account.component';
import { ReadyCouponsComponent } from './ready-coupons/ready-coupons.component';
import { UserCouponsComponent } from './user-coupons/user-coupons.component';
import { UserRatesComponent } from './user-rates/user-rates.component';
import { RiskManagementComponent } from './risk-management/risk-management.component';
import { ReportsComponent } from './reports/reports.component';
import { TransfersComponent } from './transfers/transfers.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { SendCreditComponent } from './send-credit/send-credit.component';
import { WithdrawCreditComponent } from './withdraw-credit/withdraw-credit.component';
import { CreditHistoryComponent } from './credit-history/credit-history.component';
import { AddSport } from './sports/add-sport';
import { AddCountry } from './countries/add-country';

import { SelectDropDownModule } from 'ngx-select-dropdown'

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({

  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule,
    Ng2SmartTableModule,
    ToasterModule,
    NgxDatatableModule,
    CommonModule,
    SelectDropDownModule,
    UiSwitchModule
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    SportsComponent,
    CountriesComponent,
    TournementsComponent,
    TeamsComponent,
    BetTypesComponent,
    BetSubTypesComponent,
    MatchesComponent,
    RatesComponent,
    ResultsComponent,
    DealersComponent,
    UsersComponent,
    AccountComponent,
    ReadyCouponsComponent,
    UserCouponsComponent,
    UserRatesComponent,
    RiskManagementComponent,
    ReportsComponent,
    TransfersComponent,
    SendCreditComponent,
    WithdrawCreditComponent,
    CreditHistoryComponent,
    AddSport,
    AddCountry,
    AddTournement
  ],
  entryComponents:[
    AddSport,
    AddCountry,
    AddTournement
  ],
})
export class PagesModule {
}
