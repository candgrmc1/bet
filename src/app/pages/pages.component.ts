import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  template: `

    <ngx-sample-layout>

      <nb-menu [items]="menu"></nb-menu>
  <router-outlet>
  <toaster-container></toaster-container>
  </router-outlet>

    </ngx-sample-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
}
