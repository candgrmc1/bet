import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Spor',
    group: true,
  },
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Sporlar',
    icon: 'nb-home',
    link: '/pages/sports',

  },
  {
    title: 'Ülkeler',
    icon: 'nb-home',
    link: '/pages/countries',

  },
  {
    title: 'Turnuvalar',
    icon: 'nb-home',
    link: '/pages/tournements',

  },
  {
    title: 'Takımlar',
    icon: 'nb-home',
    link: '/pages/teams',

  },
  {
    title: 'Bahis Türleri',
    icon: 'nb-home',
    link: '/pages/bet-types',

  },
  {
    title: 'Bahis Alt Tipleri',
    icon: 'nb-home',
    link: '/pages/bet-sub-types',

  },
  {
    title: 'Maçlar',
    icon: 'nb-home',
    link: '/pages/matches',

  },
  {
    title: 'Oranlar',
    icon: 'nb-e-commerce',
    link: '/pages/rates',
  },

  {
    title: 'Sonuçlar',
    icon: 'nb-home',
    link: '/pages/results',
  },
  {
    title: 'Kullanıcı',
    group: true,
  },
  {
    title: 'Bayiler',
    icon: 'nb-home',
    link: '/pages/dealers',
  },
  {
    title: 'Kullanıcılar',
    icon: 'nb-person',
    link: '/pages/users',
  },
  {
    title: 'Transfer',
    icon: 'nb-home',
    link: '/pages/transfers',
    children: [
      {
        title: 'Kredi gönder',
        link: '/pages/send-credit',
      },
      {
        title: 'Kredi çek',
        link: '/pages/withdraw-credit',
      },
      {
        title: 'Hareketler',
        link: '/pages/credit-history',
      },
    ]
  },
  {
    title: 'Hesap',
    icon: 'nb-person',
    link: '/pages/account',
  },
  {
    title: 'Hazır Kuponlar',
    icon: 'nb-compose',
    link: '/pages/ready-coupons',
  },
  {
    title: 'Kullanıcı Kuponları',
    icon: 'nb-compose',
    link: '/pages/user-coupons',
  },
  {
    title: 'Oranlar',
    icon: 'nb-bar-chart',
    link: '/pages/user-rates',
  },
  {
    title: 'Risk Yönetimi',
    icon: 'nb-compose',
    link: '/pages/risk-management',
  },
  {
    title: 'Raporlar',
    icon: 'nb-compose',
    link: '/pages/reports',
  },

];
